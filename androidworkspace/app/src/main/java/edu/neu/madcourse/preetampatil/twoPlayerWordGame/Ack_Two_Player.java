package edu.neu.madcourse.preetampatil.twoPlayerWordGame;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.preetampatil.R;

/**
 * Created by Preetam on 3/22/2015.
 */
public class Ack_Two_Player extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twogame_ack);

        Button back = (Button) findViewById(R.id.two_game_ack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

    }
}
