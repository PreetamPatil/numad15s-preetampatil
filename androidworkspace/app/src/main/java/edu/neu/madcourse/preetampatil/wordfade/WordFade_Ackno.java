package edu.neu.madcourse.preetampatil.wordfade;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.preetampatil.R;

/**
 * Created by Preetam on 2/22/2015.
 */
public class WordFade_Ackno extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.wordfade_ackno);

        Button back = (Button) findViewById(R.id.dict_back_wordfade_ackno);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();


            }
        });
    }
}
