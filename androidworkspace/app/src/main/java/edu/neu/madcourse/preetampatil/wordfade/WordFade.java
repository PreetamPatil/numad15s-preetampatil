
package edu.neu.madcourse.preetampatil.wordfade;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.logging.Handler;

import edu.neu.madcourse.preetampatil.R;


/**
 * Created by Preetam on 2/13/2015.
 */

public class WordFade extends Activity {


  //  private NewGame game;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wordfade_main);



        Button exit = (Button) findViewById(R.id.exit_button_word);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        final Button newGame =  (Button) findViewById(R.id.new_button_word);
        newGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(WordFade.this,ToNewGamePage.class);
                intent.putExtra("Player","single_player");

                if(null!=intent)
                {
                    WordFade.this.startActivity(intent);
                }
            }
        });


        Button about = (Button) findViewById(R.id.about_button_word);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent("edu.neu.madcourse.preetampatil.WORDFADE_ABOUT"));
            }
        });

        Button ack = (Button) findViewById(R.id.new_button_Ack);
        ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            startActivity(new Intent("edu.neu.madcourse.preetampatil.WORDFADE_ACKNO"));

            }
        });


    }
}