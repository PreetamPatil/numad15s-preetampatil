package edu.neu.madcourse.preetampatil.twoPlayerWordGame;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.neu.madcourse.preetampatil.R;

/**
 * Created by Preetam on 3/20/2015.
 */
public class YouLoose extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.you_loose);
        String yourScore = getIntent().getStringExtra("youscore");
        String opponentScore = getIntent().getStringExtra("opponentscore");

        Log.w("YourScore: ", yourScore);
        Log.w("OpponentScore: ",opponentScore);

        TextView your = (TextView) findViewById(R.id.you_loose_yourscore);
        TextView opponent = (TextView) findViewById(R.id.you_loose_opponentscore);

        your.setText(yourScore);
        opponent.setText(opponentScore);

        Button exit = (Button) findViewById(R.id.you_loose_exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                System.exit(0);
            }
        });


   }
}
