package edu.neu.madcourse.preetampatil.communication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.parse.Parse;

import edu.neu.madcourse.preetampatil.R;
import edu.neu.madcourse.preetampatil.twoPlayerWordGame.IsActivtyVisible;

/**
 * Created by Preetam on 3/12/2015.
 */
public class RecievedMessage extends Activity {

    TextView nameSender;
    TextView messageSender;
    private Context context;
    IsActivtyVisible check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.recieved_message);

        Parse.initialize(this, "Y2deVsLVDLdXY8ogGN7JYm7RRapXBCsoCCFXQSTG", "ac4bTzOkKn3wuu3L8dEGxrUKHXyBjFzGIp5fkfi5");

        final MediaPlayer music = MediaPlayer.create(this, R.raw.buttonmusics);

        check = new IsActivtyVisible();

        final String name = getIntent().getStringExtra("name");
        String message = getIntent().getStringExtra("message");
        final String id = getIntent().getStringExtra("regID");

        Log.w("this is value is before click: ",id);
        Log.w("this is value is before click: ",name);


        nameSender = (TextView) findViewById(R.id.sender_name);
        messageSender = (TextView) findViewById(R.id.message_sent);

        nameSender.setText(name);
        messageSender.setText(message);


        Button reply = (Button) findViewById(R.id.reply);
        reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                music.start();
                sendMessage(RecievedMessage.this, name,id);
                check.activityResumed();
                Log.w("this is value: ",id);
                Log.w("this is value: ",name);


            }
        });

        Button cancel = (Button) findViewById(R.id.back_message);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

    }

    private void sendMessage(Context context, String name, String id){

        Intent intent = new Intent(context,SendMessage.class);
        intent.putExtra("Key",name);
        intent.putExtra("Value",id);
        intent.putExtra("classname","Message");
        intent.putExtra("open_activity","true");
        Log.w("this is value: ",id);

        if(null!=intent)
        {
            context.startActivity(intent);
        }

    }
}
