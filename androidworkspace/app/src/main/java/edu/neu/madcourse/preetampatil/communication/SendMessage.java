package edu.neu.madcourse.preetampatil.communication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.preetampatil.R;
import edu.neu.madcourse.preetampatil.twoPlayerWordGame.TwoPlayerWordGame;


/**
 * Created by Preetam on 3/7/2015.
 */
public class SendMessage extends Activity {

    private String key;
    private String value;
    private EditText text;
    private String messages;
    SharedPreferences pref;
    Context context;
    CommunicationMain main;
    TwoPlayerWordGame twoPlayer;
    TextView send_Message;
    TextView send_Invite;
    String classname;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        pref = getGCMPreferences(this);

        if (getIntent().getStringExtra("open_activity").equals("false")){

            key = getIntent().getStringExtra("Key");
            value = getIntent().getStringExtra("Value");
            classname = getIntent().getStringExtra("classname");

            if (classname.equals("cancel")){
                messages = "Your invitation was cancelled";
            }
            else if (classname.equals("score")){
                messages = getIntent().getStringExtra("scoreOfPlayer");
                Log.w("This is message: ",""+messages);

            }
            else{
                messages = "Your invitation was accepted";
            }
            sendMessage(messages);
            finish();
        }else {

            setContentView(R.layout.send_message);

            send_Message = (TextView) findViewById(R.id.text_send);
            send_Invite = (TextView) findViewById(R.id.text_send1);


            key = getIntent().getStringExtra("Key");
            value = getIntent().getStringExtra("Value");
            classname = getIntent().getStringExtra("classname");


            text = (EditText) findViewById(R.id.message_id);
            Log.w("KeyValue1234: ", key + "---" + value);

            twoPlayer = new TwoPlayerWordGame();
            Log.w("This is check: ", "" + twoPlayer.returnCheckActivity());

            if (classname.equals("Invite")) {
                send_Message.setVisibility(View.GONE);
                text.setVisibility(View.GONE);
                send_Invite.setText("Send Invitation to " + key);
            } else {
                send_Invite.setVisibility(View.GONE);
            }


            Button cancel = (Button) findViewById(R.id.cancel_message);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            Button send = (Button) findViewById(R.id.send_message);
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (classname.equals("Invite")) {

                        messages = pref.getString("name", null) + " is inviting you to play WordFade";
                        sendMessage(messages);
                        onBackPressed();

                    } else {
                        messages = text.getText().toString();

                        if (messages != "") {
                            sendMessage(messages);
                        } else {
                            messages = pref.getString("name", null) + " is inviting you to play WordFade";
                        }
                        Log.w("Messagessss: ", messages);
                        onBackPressed();
                    }


                }
            });

        }

    }

    @SuppressLint("NewApi")
    private void sendMessage(final String message) {
        if (pref.getString("registration_id",null) == null || pref.getString("registration_id",null).equals("")) {
            Toast.makeText(this, "You must register first", Toast.LENGTH_LONG)
                    .show();
            return;
        }

        if (value == null || value.equals("")){
            Toast.makeText(this, "Registration ID not found...Please refresh the page", Toast.LENGTH_LONG)
                    .show();
            }
        if (message.isEmpty()) {
            Toast.makeText(this, "Empty Message", Toast.LENGTH_LONG).show();
            return;
        }

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                List<String> regIds = new ArrayList<String>();
                String reg_device = value;
                //  int nIcon = R.drawable.ic_stat_cloud;
                int nType = CommunicationConstants.SIMPLE_NOTIFICATION;
                Map<String, String> msgParams;
                msgParams = new HashMap<String, String>();
                msgParams.put("data.alertText", "Message from: "+pref.getString("name",null));
                msgParams.put("data.titleText", "Message from: "+pref.getString("name",null));
                Log.w("This is message: ", messages + "-----------" + value);
                msgParams.put("data.contentText", messages);
                msgParams.put("data.name", pref.getString("name",null));
                msgParams.put("data.REG_ID",pref.getString("registration_id",null));
                msgParams.put("data.checkActivity",classname);
                // msgParams.put("data.nIcon", String.valueOf(nIcon));
                msgParams.put("data.nType", String.valueOf(nType));
                setSendMessageValues(messages);
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds,
                        SendMessage.this);
                msg = "sending information...";
                return msg;
            }

           /* @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }*/
        }.execute(null, null, null);
    }


    private static void setSendMessageValues(String msg) {
        CommunicationConstants.alertText = "Message Notification";
        CommunicationConstants.titleText = "Sending Message";
        CommunicationConstants.contentText = msg;
//        Log.w("This is the msg:",msg);

    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(CommunicationMain.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

}