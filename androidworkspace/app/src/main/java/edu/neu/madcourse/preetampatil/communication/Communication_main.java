package edu.neu.madcourse.preetampatil.communication;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.preetampatil.R;


/**
 * Created by Preetam on 2/27/2015.
 */
public class Communication_main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.communication_main);

     final MediaPlayer music = MediaPlayer.create(this, R.raw.buttonmusics);



        Button exit = (Button) findViewById(R.id.communication_back);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                music.start();
                onBackPressed();
            }
        });


        Button register = (Button) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                music.start();
                startActivity(new Intent("edu.neu.madcourse.preetampatil.COMMUNICATIONMAIN"));

            }
        });

        Button parse = (Button) findViewById(R.id.parse_data);
        parse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
              //  startActivity(new Intent("edu.neu.madcourse.preetampatil.PARSE_SAMPLE"));

                Intent intent = new Intent(Communication_main.this,ParseSample.class);
                intent.putExtra("classname","Message");

                if(null!=intent)
                {

                    Communication_main.this.startActivity(intent);
                }
              }
        });


        Button ack = (Button) findViewById(R.id.ack_comm);
        ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
                startActivity(new Intent("edu.neu.madcourse.preetampatil.COMMUNICATION_ACK"));
            }
        });

    }
}
