package edu.neu.madcourse.preetampatil.twoPlayerWordGame;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.neu.madcourse.preetampatil.communication.ParseKeyValue;

/**
 * Created by Preetam on 3/21/2015.
 */
public class PutInParse extends Activity{

     HashMap<String,String> myMap;
     ArrayList<String> array2;
     ArrayList<String> array3;
    int temp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this, "Y2deVsLVDLdXY8ogGN7JYm7RRapXBCsoCCFXQSTG", "ac4bTzOkKn3wuu3L8dEGxrUKHXyBjFzGIp5fkfi5");

      /*  String name = getIntent().getStringExtra("name");
        String score = getIntent().getStringExtra("score");*/

        temp = 0;

        array2 = new ArrayList<String>();
        array3 = new ArrayList<String>();
         myMap = new HashMap<String,String>();

        ParseObject parseObject = new ParseObject("TOP_SCORE_LIST");
        parseObject.put("Jadu",7);
        parseObject.saveInBackground();

       ParseQuery<ParseObject> query = ParseQuery.getQuery("TOP_SCORE_LIST");

         final ArrayList<ParseKeyValue> array1 = new ArrayList<>();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                Log.w("ParseObject: ", "" + parseObjects.size());

                if (e == null) {
                    for (ParseObject parseObject : parseObjects) {
                        for (String key : parseObject.keySet()) {
                            ParseKeyValue item = new ParseKeyValue(key, parseObject.getString(key));
                            Log.w("item name: ",item.getKey());
                            Log.w("item value: ",""+parseObject.get(key));

                            array1.add(item);
                            array2.add(item.getKey());
                            array3.add(parseObject.get(key).toString());

                            myMap.put(item.getKey().toString(),parseObject.get(key).toString());

                        }
                    }

                } else {
                    Toast.makeText(PutInParse.this, "Failed to load data from Parse", Toast.LENGTH_LONG).show();
                }
            }


        });



        finish();

    }
}
