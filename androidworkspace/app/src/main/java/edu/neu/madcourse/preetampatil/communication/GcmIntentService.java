package edu.neu.madcourse.preetampatil.communication;

/**
 * Created by Preetam on 2/28/2015.
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import edu.neu.madcourse.preetampatil.R;
import edu.neu.madcourse.preetampatil.twoPlayerWordGame.AcceptCancel;
import edu.neu.madcourse.preetampatil.twoPlayerWordGame.GameRequest;
import edu.neu.madcourse.preetampatil.twoPlayerWordGame.IsActivtyVisible;
import edu.neu.madcourse.preetampatil.wordfade.ToNewGamePage;

public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    static final String TAG = "GCM_Communication";
    String checkActivity;
    Intent myintent;
    boolean checkFalg = false;


    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String alertText = CommunicationConstants.alertText;
        String titleText = CommunicationConstants.titleText;
        String contentText = CommunicationConstants.contentText;
        String name;
        String regID;
        IsActivtyVisible checkFlag;

        Bundle extras = intent.getExtras();

        checkFlag = new IsActivtyVisible();
        Log.d("Bundle value: "+String.valueOf(extras.size()), extras.toString());

        if (!extras.isEmpty()) {

            alertText = extras.getString("alertText");
            titleText = extras.getString("titleText");
            contentText = extras.getString("contentText");
            name = extras.getString("name");
            regID = extras.getString("REG_ID");
            checkActivity = extras.getString("checkActivity");


            if (contentText == null){
            }else {

                if (checkFlag.isActivityVisible()){

                int value;

                if (contentText.equals("game_over")){
                    ToNewGamePage.opponentGameOverText();
                }else{
                    if (contentText.length()<=2) {
                        value = Integer.parseInt(contentText);
                        ToNewGamePage.setValue(value);
                    }
                }

                }else {
                    sendNotification(alertText, titleText, contentText, name, regID);
                }
                }

        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    public void sendNotification(String alertText, String titleText,
                                 String contentText, String name, String regID) {

        mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent;

        if (checkActivity.equals("Invite")){

            notificationIntent = new Intent(this,
                    GameRequest.class);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            notificationIntent.putExtra("show_response", "show_response");

            myintent = new Intent(this, GameRequest.class);

        }else if (checkActivity.equals("accept") || checkActivity.equals("cancel")){

            notificationIntent = new Intent(this,
                    AcceptCancel.class);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            notificationIntent.putExtra("show_response", "show_response");

            myintent = new Intent(this, AcceptCancel.class);
        }else if (checkActivity.equals("score")){

            notificationIntent = new Intent(this,
                    ToNewGamePage.class);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            notificationIntent.putExtra("Player","multi_player");

            checkFalg = true;

           myintent = new Intent(this, ToNewGamePage.class);


        }

        else{
            notificationIntent = new Intent(this,
                    RecievedMessage.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               notificationIntent.putExtra("show_response", "show_response");

            myintent = new Intent(this, RecievedMessage.class);
        }

        if (checkFalg){

            myintent.putExtra("Player","multi_player");
            myintent.putExtra("key", name);
            myintent.putExtra("value", regID);
            checkFalg = false;

            if (contentText.equals("game_over")){
                ToNewGamePage.opponentGameOverText();
            }else {
                int value;
                value = Integer.parseInt(contentText);
                ToNewGamePage.setValue(value);
            }

        }else {
            myintent.putExtra("message", contentText);
            myintent.putExtra("name", name);
            myintent.putExtra("regID", regID);
            myintent.putExtra("check_activity",checkActivity);
        }

        PendingIntent intent = PendingIntent.getActivity(this, 0, myintent ,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this)
                .setSmallIcon(R.drawable.ic_stat_cloud)
                .setContentTitle(titleText)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                                .bigText(contentText))
                .setContentText(contentText).setTicker(alertText)
                .setAutoCancel(true);
        mBuilder.setContentIntent(intent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}