package edu.neu.madcourse.preetampatil.wordfade;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.preetampatil.R;

/**
 * Created by Preetam on 2/19/2015.
 */
public class TimeOver extends Dialog {


    private ToNewGamePage toNewGamePage;


    public TimeOver (Context context) {
        super(context);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeout);

        toNewGamePage = new ToNewGamePage();


        Button over = (Button) findViewById(R.id.game_over);
        over.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });


    }


}
