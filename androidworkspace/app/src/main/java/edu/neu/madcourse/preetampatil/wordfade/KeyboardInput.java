
package edu.neu.madcourse.preetampatil.wordfade;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import edu.neu.madcourse.preetampatil.R;

public class KeyboardInput extends Dialog {


    private View alphaKey;
    private final Button aplha[] = new Button[22];
    static ToNewGamePage toNewGamePage;
    Vibrator vibe;
    final String [] alphabets = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P",
            "Q","R","S","T","U","V","W","X","Y","Z"};
    static int [] alphabetsCount = {9,2,2,4,12,2,3,2,9,1,1,4,2,6,8,2,1,6,4,6,4,2,2,1,2,1};
    static int [] randomCount = new int[26];
    String [] randomAlphabet;
    static Random random;
    static int count;
    static int counter = 0;
    static List<Integer> checkInvisible;
    static int increment = 0;
    static String newText [];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alphabet_table);
//        toNewGamePage = new ToNewGamePage();

        vibe = (Vibrator)getContext().getSystemService(Context.VIBRATOR_SERVICE);
        checkInvisible = new ArrayList<Integer>();
        newText = new String[3];

        final Button back = (Button)findViewById(R.id.keypad_backs);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        randomAlphabet = new String[22];
        random = new Random();

        setText();
        findKeyID();
        listners();

    }

    public KeyboardInput(Context context, ToNewGamePage gm) {
        super(context);
        toNewGamePage = gm;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (toNewGamePage.setSplitflag) {

            if (checkInvisible.size() > 0) {
                String x = toNewGamePage.splitText();
                aplha[checkInvisible.get(0)].setText(x);
                randomAlphabet[checkInvisible.get(0)] = x;
                aplha[checkInvisible.get(0)].setVisibility(View.VISIBLE);
                checkInvisible.remove(0);
            }

        }

        if (toNewGamePage.dumpFlag) {

            if (checkInvisible.size() > 0) {
                newText = toNewGamePage.getNewText();

                for (int i = 0; i < newText.length; i++){

                    aplha[checkInvisible.get(0)].setText(newText[i]);
                    randomAlphabet[checkInvisible.get(0)] = newText[i];
                    aplha[checkInvisible.get(0)].setVisibility(View.VISIBLE);
                    checkInvisible.remove(0);
                }

            }

        }


    }

    private void setText(){

        counter = 0;
        Arrays.fill(randomCount,0);
        while (counter < 22) {
            int r = random.nextInt(alphabets.length);

            if (randomCount[r] < alphabetsCount[r]) {
                randomCount[r] = randomCount[r] + 1;
                randomAlphabet[counter] = alphabets[r];
                counter++;
            }
        }

    }

    private void findKeyID() {

        alphaKey = findViewById(R.id.keypad_wordgame);

        aplha[0] = (Button)findViewById(R.id.keypad_A);
        aplha[0].setText(randomAlphabet[0]);

        aplha[1] = (Button)findViewById(R.id.keypad_B);
        aplha[1].setText(randomAlphabet[1]);

        aplha[2] = (Button)findViewById(R.id.keypad_C);
        aplha[2].setText(randomAlphabet[2]);

        aplha[3] = (Button)findViewById(R.id.keypad_C);
        aplha[3].setText(randomAlphabet[3]);

        aplha[4] = (Button)findViewById(R.id.keypad_D);
        aplha[4].setText(randomAlphabet[4]);

        aplha[5] = (Button)findViewById(R.id.keypad_E);
        aplha[5].setText(randomAlphabet[5]);

        aplha[6] = (Button)findViewById(R.id.keypad_F);
        aplha[6].setText(randomAlphabet[6]);

        aplha[7] = (Button)findViewById(R.id.keypad_G);
        aplha[7].setText(randomAlphabet[7]);

        aplha[8] = (Button)findViewById(R.id.keypad_H);
        aplha[8].setText(randomAlphabet[8]);

        aplha[9] = (Button)findViewById(R.id.keypad_I);
        aplha[9].setText(randomAlphabet[9]);

        aplha[10] = (Button)findViewById(R.id.keypad_J);
        aplha[10].setText(randomAlphabet[10]);

        aplha[11] = (Button)findViewById(R.id.keypad_K);
        aplha[11].setText(randomAlphabet[11]);

        aplha[12] = (Button)findViewById(R.id.keypad_L);
        aplha[12].setText(randomAlphabet[12]);

        aplha[13] = (Button)findViewById(R.id.keypad_M);
        aplha[13].setText(randomAlphabet[13]);

        aplha[14] = (Button)findViewById(R.id.keypad_N);
        aplha[14].setText(randomAlphabet[14]);

        aplha[15] = (Button)findViewById(R.id.keypad_O);
        aplha[15].setText(randomAlphabet[15]);

        aplha[16] = (Button)findViewById(R.id.keypad_P);
        aplha[16].setText(randomAlphabet[16]);

        aplha[17] = (Button)findViewById(R.id.keypad_Q);
        aplha[17].setText(randomAlphabet[17]);

        aplha[18] = (Button)findViewById(R.id.keypad_R);
        aplha[18].setText(randomAlphabet[18]);

        aplha[19] = (Button)findViewById(R.id.keypad_S);
        aplha[19].setText(randomAlphabet[19]);

        aplha[20] = (Button)findViewById(R.id.keypad_T);
        aplha[20].setText(randomAlphabet[20]);

        aplha[21] = (Button)findViewById(R.id.keypad_U);
        aplha[21].setText(randomAlphabet[21]);

    }


    private void listners() {

        for ( count = 0; count < aplha.length ; count++) {

            final int x = count;
            aplha[count].setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    final String word = randomAlphabet[x];
                    Log.w("Selected word", word + "");
                    vibe.vibrate(100);
                    Log.w("Alpha", x + "");
                    randomAlphabet[x] = "";
                    aplha[x].setVisibility(View.INVISIBLE);
                    checkInvisible.add(x);
                    increment++;
                    returnChar(word);
                }
            });

        }

        alphaKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }



    private void returnChar (String charc){
        Log.w("Selected word in return char: ",charc+"");
        toNewGamePage.setLetter(charc);
        dismiss();
    }


}