package edu.neu.madcourse.preetampatil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Preetam on 1/15/2015.
 */
public class AboutDevloper extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final MediaPlayer music = MediaPlayer.create(this, R.raw.buttonmusics);

        setContentView(R.layout.aboutdeveloper);

        TextView imeiID =  (TextView) findViewById(R.id.imeiID);
        TelephonyManager imei = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (imei.getDeviceId()!=null){
            imeiID.setText("IMEI No:"+imei.getDeviceId()+"\n");
        }
        else
        {
            imeiID.setText("IMEI number not found"+"\n");
        }

        Button back = (Button) findViewById(R.id.backbt);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              music.start();
              onBackPressed();

            }
        });
     }
}
