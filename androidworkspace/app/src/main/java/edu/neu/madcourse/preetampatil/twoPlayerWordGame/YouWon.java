package edu.neu.madcourse.preetampatil.twoPlayerWordGame;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.neu.madcourse.preetampatil.R;

/**
 * Created by Preetam on 3/19/2015.
 */
public class YouWon extends Activity {


        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.you_won);

        String yourScore = getIntent().getStringExtra("youscore");
        String opponentScore = getIntent().getStringExtra("opponentscore");

//      Log.w("YourScore: ",yourScore);
        Log.w("OpponentScore: ",opponentScore);

        TextView your = (TextView) findViewById(R.id.you_won_yourscore);
        TextView opponent = (TextView) findViewById(R.id.you_won_opponentscore);

        your.setText(yourScore);
        opponent.setText(opponentScore);

        Button exit = (Button) findViewById(R.id.you_won_exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);/*//***Change Here***
                startActivity(intent);
                finish();
                System.exit(0);*/
                finish();
                System.exit(0);


            }
        });
    }

}
