package edu.neu.madcourse.preetampatil.twoPlayerWordGame;

import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import edu.neu.madcourse.preetampatil.wordfade.ToNewGamePage;

/**
 * Created by Preetam on 3/20/2015.
 */
public class MySensor {

    int count;
    ToNewGamePage toNewGamePage;

    public MySensor(ToNewGamePage newGame) {
        this.toNewGamePage = newGame;
    }

    /* The SensorEventListener */
    public SensorEventListener mySensorEventListener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent se) {
            updateAccelParameters(se.values[0], se.values[1], se.values[2]);
            if ((!shakeInitiated) && isAccelerationChanged()) {
                shakeInitiated = true;
            } else if ((shakeInitiated) && isAccelerationChanged()) {
                executeShakeAction();
            } else if ((shakeInitiated) && (!isAccelerationChanged())) {
                shakeInitiated = false;
            }
        }

        @Override
        public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {

        }


        /*current values of acceleration*/
        private float xAccel;
        private float yAccel;
        private float zAccel;

        /*previous values of acceleration */
        private float xPreviousAccel;
        private float yPreviousAccel;
        private float zPreviousAccel;

        /*suppress the first shaking */
        private boolean firstUpdate = true;


        //for normal shake movement 5.5
        private final float shakeThreshold = 5.5f;


        private boolean shakeInitiated = false;


        private void updateAccelParameters(float xNewAccel, float yNewAccel,
                                           float zNewAccel) {
            if (firstUpdate) {
                xPreviousAccel = xNewAccel;
                yPreviousAccel = yNewAccel;
                zPreviousAccel = zNewAccel;
                firstUpdate = false;
            } else {
                xPreviousAccel = xAccel;
                yPreviousAccel = yAccel;
                zPreviousAccel = zAccel;
            }
            xAccel = xNewAccel;
            yAccel = yNewAccel;
            zAccel = zNewAccel;
        }


        private boolean isAccelerationChanged() {
            float deltaX = Math.abs(xPreviousAccel - xAccel);
            float deltaY = Math.abs(yPreviousAccel - yAccel);
            float deltaZ = Math.abs(zPreviousAccel - zAccel);
            return (deltaX > shakeThreshold && deltaY > shakeThreshold)
                    || (deltaX > shakeThreshold && deltaZ > shakeThreshold)
                    || (deltaY > shakeThreshold && deltaZ > shakeThreshold);
        }

        private void executeShakeAction() {
            toNewGamePage.shakeTile();
        }

    };
}

