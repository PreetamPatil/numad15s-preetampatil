package edu.neu.madcourse.preetampatil.communication;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.preetampatil.R;


/**
 * Created by Preetam on 3/6/2015.
 */
public class ParseSample extends Activity implements View.OnClickListener  {

    private static final String PARSE_TEST_OBJECT = "PARSE_TEST_OBJECT";

    private ListView lvKeyValuePairs = null;
    private EditText etKey = null;
    private EditText etValue = null;
    SharedPreferences pref;
    String username;
    MediaPlayer music;
    Button back;
    String checkActvity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parse_sample_main);
        lvKeyValuePairs = (ListView) findViewById(R.id.lv_cognito_dev_auth_sample);
        findViewById(R.id.btn_cognito_dev_auth_sample_refresh).setOnClickListener(this);

        checkActvity = getIntent().getStringExtra("classname");

         music = MediaPlayer.create(this, R.raw.buttonmusics);

        back = (Button) findViewById(R.id.register_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
                onBackPressed();

            }
        });
        pref = getGCMPreferences(this);
        username = pref.getString("name",null);

        // Enable Local Datastore.
        Parse.initialize(this, "Y2deVsLVDLdXY8ogGN7JYm7RRapXBCsoCCFXQSTG", "ac4bTzOkKn3wuu3L8dEGxrUKHXyBjFzGIp5fkfi5");
        onRefresh();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cognito_dev_auth_sample_refresh:
               music.start();
                onRefresh();
                break;

        }
    }

    private void onRefresh() {
/*////////////////Query to clear parse data ///////////////////
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PARSE_TEST_OBJECT");
        final ArrayList<ParseKeyValue> array = new ArrayList<>();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    for (ParseObject parseObject : parseObjects) {
                        //delete all parse values
                        try {
                            parseObject.delete();
                        } catch (ParseException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });

   ///////////////////////////////*/

        ParseQuery<ParseObject> query = ParseQuery.getQuery(PARSE_TEST_OBJECT);
        final ArrayList<ParseKeyValue> array = new ArrayList<>();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    for (ParseObject parseObject : parseObjects) {
                        for (String key : parseObject.keySet()) {
                            ParseKeyValue item = new ParseKeyValue(key, parseObject.getString(key));
                            array.add(item);
                        }
                    }
                    updateList(array);
                } else {
                    Toast.makeText(ParseSample.this, "Failed to load data from Parse", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void updateList(ArrayList<ParseKeyValue> data) {
        ParseKeyValueAdapter adapter = new ParseKeyValueAdapter(this, data, username, checkActvity);
        lvKeyValuePairs.setAdapter(adapter);
        lvKeyValuePairs.invalidate();
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(CommunicationMain.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

}
