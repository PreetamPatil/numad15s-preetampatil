package edu.neu.madcourse.preetampatil.twoPlayerWordGame;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.neu.madcourse.preetampatil.R;
import edu.neu.madcourse.preetampatil.communication.SendMessage;
import edu.neu.madcourse.preetampatil.wordfade.ToNewGamePage;

/**
 * Created by Preetam on 3/17/2015.
 */
public class GameRequest extends Activity {

    TextView setSenderText;
    IsActivtyVisible check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_request);

        check = new IsActivtyVisible();
        final MediaPlayer music = MediaPlayer.create(this, R.raw.buttonmusics);

        final String name = getIntent().getStringExtra("name");
        String message = getIntent().getStringExtra("message");
        final String id = getIntent().getStringExtra("regID");

        setSenderText = (TextView) findViewById(R.id.request_sender);
        setSenderText.setText(message);

        Button cancel = (Button) findViewById(R.id.request_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                music.start();
                sendCancelReply(GameRequest.this, name, id);
                onBackPressed();

             }
        });


        Button accept = (Button) findViewById(R.id.request_accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   music.start();
                check.activityResumed();
                sendAcceptReply(GameRequest.this,name,id);
                onBackPressed();

            }
        });
    }


    private void sendCancelReply(Context context, String name, String id){
        Intent intent = new Intent(context,SendMessage.class);
        intent.putExtra("Key",name);
        intent.putExtra("Value",id);
        intent.putExtra("open_activity","false");
        intent.putExtra("classname","cancel");

        if(null!=intent)
        {
            context.startActivity(intent);
        }

    }

    private void sendAcceptReply (Context context, String name, String id){

        Intent intent = new Intent(context,ToNewGamePage.class);
        intent.putExtra("Player","multi_player");
        intent.putExtra("Key",name);
        intent.putExtra("Value",id);

        if(null!=intent)
        {
            context.startActivity(intent);
        }

        Intent intent_accept = new Intent(context,SendMessage.class);
        intent_accept.putExtra("Key",name);
        intent_accept.putExtra("Value",id);
        intent_accept.putExtra("classname","accept");
        intent_accept.putExtra("open_activity","false");

        if(null!=intent_accept)
        {
            context.startActivity(intent_accept);
        }



    }


}
