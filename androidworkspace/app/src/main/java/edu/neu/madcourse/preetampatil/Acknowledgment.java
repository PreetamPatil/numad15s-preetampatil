package edu.neu.madcourse.preetampatil;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Preetam on 1/21/2015.
 */
public class Acknowledgment extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final MediaPlayer music = MediaPlayer.create(this, R.raw.buttonmusics);
        setContentView(R.layout.ackn_layout);


        Button back = (Button) findViewById(R.id.ackb);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                music.start();
                onBackPressed();

            }
        });


    }
}
