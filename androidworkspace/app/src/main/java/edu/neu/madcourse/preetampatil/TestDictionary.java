package edu.neu.madcourse.preetampatil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import edu.neu.madcourse.preetampatil.wordfade.ToNewGamePage;

/**
 * Created by Preetam on 1/24/2015.
 */
public class TestDictionary extends Activity {

    EditText inText;
    TextView outText;
    SharedPreferences storeData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE); //For customizing my title bar

        final MediaPlayer beepMusic = MediaPlayer.create(this, R.raw.beep);
        final MediaPlayer buttonMusic = MediaPlayer.create(this, R.raw.buttonmusics);

        final ProgressDialog progress;



        storeData = MainMenu.returnSharedPref();


        if (storeData == null){
            storeData = ToNewGamePage.returnSharedPref();
        }


       progress = ProgressDialog.show(this,"Loading","Please wait data is loading",true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {

                    if (!storeData.getString(""+"z", null).equals(null))
                    {
                      break;
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                    }
                });
            }
        }).start();

        setContentView(R.layout.dictionary);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dict_title);


        Button back = (Button) findViewById(R.id.dictBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonMusic.start();
                onBackPressed();

            }
        });


        inText = (EditText) findViewById(R.id.dict_inText);
        outText = (TextView) findViewById(R.id.dict_outText);


        Button clear = (Button) findViewById(R.id.dictClear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonMusic.start();
                inText.setText("");
                outText.setText("");

            }
        });

        Button ack = (Button) findViewById(R.id.dictAck);
        ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonMusic.start();
                startActivity(new Intent("edu.neu.madcourse.preetampatil.ACKDICTIONARY"));

            }
        });




        inText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() > 0) {

                    char a = editable.toString().toLowerCase().charAt(0);

                    if (inText.getText().toString().length() >= 3) {
                        String words = storeData.getString(""+a, null);
                        List<String> listWord = Arrays.asList(words.split(","));

                        if ((listWord.contains(" " + editable.toString().toLowerCase()))) {
                            if (outText.getText().toString().contains(inText.getText().toString())) {
                            } else {
                                beepMusic.start();
                                outText.setText(outText.getText() + "\n" + editable);

                            }
                        }
                    }

                }

            }
        });
    }
}