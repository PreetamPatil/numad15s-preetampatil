package edu.neu.madcourse.preetampatil.twoPlayerWordGame;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.neu.madcourse.preetampatil.R;
import edu.neu.madcourse.preetampatil.wordfade.ToNewGamePage;

/**
 * Created by Preetam on 3/17/2015.
 */
public class AcceptCancel extends Activity {

    IsActivtyVisible check;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.accept_cancel);

        check = new IsActivtyVisible();
        final MediaPlayer music = MediaPlayer.create(this, R.raw.buttonmusics);

        final String name = getIntent().getStringExtra("name");
        String message = getIntent().getStringExtra("message");
        final String id = getIntent().getStringExtra("regID");
        String reply = getIntent().getStringExtra("check_activity");


        Button accept = (Button) findViewById(R.id.play_accept);
        Button cancel = (Button) findViewById(R.id.cancel_play);
        TextView msg = (TextView) findViewById(R.id.request_sender);

        if (reply.equals("cancel")){
            accept.setVisibility(View.GONE);
            msg.setText(message);
        }else{
            cancel.setVisibility(View.GONE);
            msg.setText(message);
        }


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
                onBackPressed();
            }
        });

       accept.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               music.start();
               check.activityResumed();
               Intent intent = new Intent(AcceptCancel.this,ToNewGamePage.class);
               intent.putExtra("Player","multi_player");
               intent.putExtra("Key",name);
               intent.putExtra("Value",id);

               if(null!=intent)
               {
                   AcceptCancel.this.startActivity(intent);
               }

               onBackPressed();

           }
       });

    }
}
