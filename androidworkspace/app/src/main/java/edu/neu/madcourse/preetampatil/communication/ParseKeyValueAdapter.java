package edu.neu.madcourse.preetampatil.communication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.preetampatil.R;


/**
 * Created by Preetam on 3/6/2015.
 */
public class ParseKeyValueAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ParseKeyValue> data;
    SharedPreferences pref;
    String username;
    String checkActivity;


    public ParseKeyValueAdapter(Context context, ArrayList<ParseKeyValue> data, String username, String checkActivity) {
        this.context = context;
        this.data = data;
        this.username = username;
        this.checkActivity = checkActivity;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public ParseKeyValue getItem(int position) {
        return data == null || position >= data.size() ? null : data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data == null || position >= data.size() ? -1 : position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_cognito_keyvalue, null);
        } else
            view = convertView;
        final TextView tvKey = (TextView) view.findViewById(R.id.tv_item_cognito_key);
        final TextView tvValue = (TextView) view.findViewById(R.id.tv_item_cognito_value);
        final Button sendMessage = (Button) view.findViewById(R.id.send_message);
        ParseKeyValue keyValue = getItem(position);
        tvKey.setText(keyValue.getKey());
        tvValue.setText(keyValue.getValue());

        if (keyValue.getKey().equalsIgnoreCase(username)) {
            //user name_number
            tvKey.setVisibility(View.GONE);
            //user regid
            tvValue.setVisibility(View.GONE);
            sendMessage.setVisibility(View.GONE);
        }


        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,SendMessage.class);
                intent.putExtra("Key",tvKey.getText());
                intent.putExtra("Value",tvValue.getText());
                intent.putExtra("classname",checkActivity);
                intent.putExtra("open_activity","true");

                if(null!=intent)
                {

                       context.startActivity(intent);
                }

            }
        });

        return view;
    }



}
