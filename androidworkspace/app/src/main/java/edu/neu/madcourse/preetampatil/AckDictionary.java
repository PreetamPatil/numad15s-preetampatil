package edu.neu.madcourse.preetampatil;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.preetampatil.R;

/**
 * Created by Preetam on 2/8/2015.
 */
public class AckDictionary extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ackn_dictionary);

        final MediaPlayer buttonMusic = MediaPlayer.create(this, R.raw.buttonmusics);


        Button back = (Button) findViewById(R.id.dict_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonMusic.start();
                onBackPressed();

            }
        });



    }
}
