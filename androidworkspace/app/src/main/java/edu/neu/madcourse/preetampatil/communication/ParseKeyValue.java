package edu.neu.madcourse.preetampatil.communication;

/**
 * Created by Preetam on 3/6/2015.
 */
public class ParseKeyValue {

    private String key;
    private String value;

    public ParseKeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
