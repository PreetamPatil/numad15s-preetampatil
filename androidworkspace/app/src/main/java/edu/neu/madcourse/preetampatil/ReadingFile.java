package edu.neu.madcourse.preetampatil;

import android.content.SharedPreferences;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


/**
 * Created by Preetam on 1/25/2015.
 */
public class ReadingFile implements Runnable {


    int count; // To initialize the ArrayList array from 0
    String fileWord; // To store the word read from the file using BufferedReader
    char alphabet; // To create a ArrayList containing different words
    InputStream inputS;
    ArrayList<String> list;
    static SharedPreferences storeData;
    static int flag;
    SharedPreferences.Editor editor;

    public ReadingFile(InputStream inputsS, SharedPreferences.Editor editor, SharedPreferences storeData ) {

        this.inputS = inputsS;
        this.editor = editor;
        this.storeData = MainMenu.returnSharedPref();
        list =  new ArrayList<String>();
        alphabet = 'a';
        count = 0;
        flag = 0;

    }

    public static SharedPreferences getStoreData() {
        return storeData;
    }

    public  static  int setFlag() {
        return flag;
    }

    @Override
    public void run() {

        //Code to read the file from the Raw folder and create a ArrayList and HashMap of the same starts


        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputS)); //Reading the file

            final long start = System.nanoTime();
            while ( (fileWord = reader.readLine()) != null) {

                if (fileWord.toLowerCase().charAt(0) != alphabet) {

                    editor.putString("" + alphabet, list.toString());
                    editor.commit();
                    alphabet = fileWord.toLowerCase().charAt(0);
                    list.clear();
                    list.add(fileWord);

                 } else {
                    list.add(fileWord);

                }

            }
            editor.putString("" +'z', list.toString());
            editor.commit();
            list.clear();
            flag = 1;
            reader.close();

            final long end = System.nanoTime();
            Log.w("Time: ",+end-start+"");
            Thread.currentThread().isInterrupted();



        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
