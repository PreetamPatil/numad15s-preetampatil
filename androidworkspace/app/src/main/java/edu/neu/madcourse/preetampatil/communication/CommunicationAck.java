package edu.neu.madcourse.preetampatil.communication;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.preetampatil.R;

/**
 * Created by Preetam on 3/13/2015.
 */
public class CommunicationAck extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.communication_ack);

       final MediaPlayer music = MediaPlayer.create(this, R.raw.buttonmusics);


        Button back = (Button) findViewById(R.id.com_back_ackno);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
                onBackPressed();
            }
        });



    }
}
