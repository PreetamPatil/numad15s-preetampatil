package edu.neu.madcourse.preetampatil.twoPlayerWordGame;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.preetampatil.R;
import edu.neu.madcourse.preetampatil.communication.ParseSample;

/**
 * Created by Preetam on 3/16/2015.
 */
public class TwoPlayerWordGame extends Activity {

    boolean checkActivityFlag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two_player_main);
        final MediaPlayer music = MediaPlayer.create(this, R.raw.buttonmusics);

        checkActivityFlag = false;

        Button register = (Button) findViewById(R.id.two_player_register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
                startActivity(new Intent("edu.neu.madcourse.preetampatil.COMMUNICATIONMAIN"));
            }
        });

        Button invite = (Button) findViewById(R.id.two_player_invite);
        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
                Intent intent = new Intent(TwoPlayerWordGame.this,ParseSample.class);
                intent.putExtra("classname","Invite");
                intent.putExtra("open_activity","true");

                if(null!=intent)
                {

                    TwoPlayerWordGame.this.startActivity(intent);
                }


            }
        });

        Button exit = (Button) findViewById(R.id.two_player_back);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
                onBackPressed();

            }
        });


        Button ack = (Button) findViewById(R.id.two_player_ack);
        ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
               startActivity(new Intent("edu.neu.madcourse.preetampatil.ACK_TWO_PLAYER"));

            }
        });

    }

    public void checkActivity(){

        Log.w("I am called: ",""+checkActivityFlag);

        if (checkActivityFlag){
            checkActivityFlag = false;
        }else{
            checkActivityFlag = true;
        }

    }

    public boolean returnCheckActivity(){

        return checkActivityFlag;
    }
}
