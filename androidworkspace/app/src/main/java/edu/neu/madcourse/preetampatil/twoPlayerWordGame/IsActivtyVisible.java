package edu.neu.madcourse.preetampatil.twoPlayerWordGame;

/**
 * Created by Preetam on 3/18/2015.
 */
public class IsActivtyVisible {

 private static boolean activityVisible;


    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

}