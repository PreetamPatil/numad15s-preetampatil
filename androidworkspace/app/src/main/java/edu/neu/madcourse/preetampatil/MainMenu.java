package edu.neu.madcourse.preetampatil;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import java.io.InputStream;

/**
 * Created by Preetam on 1/15/2015.
 */




public class MainMenu extends Activity {
    //this is main menu

    AssetManager asset;
    ReadingFile read;
    InputStream inputS;
    Thread readingThread;
    static SharedPreferences storeData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE); //For customizing my title bar

        final MediaPlayer music = MediaPlayer.create(this, R.raw.buttonmusics);
        int mode = getResources().getConfiguration().orientation;

        if (mode == 1) {
            setContentView(R.layout.main_page);
        } else {
            setContentView(R.layout.main_page_landscape);

        }


       getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.mytitle);


        storeData = getPreferences();
        SharedPreferences.Editor editor = this.getPreferences(Activity.MODE_PRIVATE).edit();

        inputS = getResources().openRawResource(R.raw.wordlist);
        read = new ReadingFile(inputS,editor,storeData);

        String arrayString = storeData.getString("z", null);

        if (arrayString == null) {

            readingThread = new Thread(read);
            readingThread.setPriority(Thread.MAX_PRIORITY);
            readingThread.start();
        }

        final Button aboutDev = (Button) findViewById(R.id.Babout);
        aboutDev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                music.start();//button music will start
                startActivity(new Intent("edu.neu.madcourse.preetampatil.ABOUTDEVELOPER"));
            }
        });


        Button sudoku = (Button) findViewById(R.id.Bsudoku);
        sudoku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                music.start();
                startActivity(new Intent("edu.neu.madcourse.preetampatil.sudoku.SUDOKU"));
            }
        });


        Button exit = (Button) findViewById(R.id.Bquit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                music.start();
                finish();
                System.exit(0);
            }
        });

        Button dictButton = (Button) findViewById(R.id.dictonaryButton);
        dictButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                music.start();
                startActivity(new Intent("edu.neu.madcourse.preetampatil.DICTIONARY"));

            }
        });


        Button btn = (Button) findViewById(R.id.error_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                music.start();
                Toast showError = Toast.makeText(MainMenu.this, "Error Generated", Toast.LENGTH_LONG);
                showError.show();
                startActivity(new Intent("a"));

                  }
        });

        Button ackButton = (Button) findViewById(R.id.ackbutton);
        ackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                music.start();
            startActivity(new Intent("edu.neu.madcourse.preetampatil.ACKNO"));
            }
        });




        Button wordGame = (Button) findViewById(R.id.wordgameButton);
        wordGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                music.start();
                startActivity(new Intent("edu.neu.madcourse.preetampatil.WORDGAME"));
            }
        });

        Button communication = (Button) findViewById(R.id.communication);
        communication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                music.start();
                startActivity(new Intent("edu.neu.madcourse.preetampatil.COMMUNICATION_MAIN"));

            }
        });

        Button multi = (Button) findViewById(R.id.two_player);
        multi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
                startActivity(new Intent("edu.neu.madcourse.preetampatil.TWO_PLAYER"));


            }
        });


        Button trickiest = (Button) findViewById(R.id.trickiest_part);
        trickiest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                music.start();
             //   startActivity(new Intent("edu.neu.madcourse.preetampatil.TRICKIEST_PART"));

                startActivity(new Intent("edu.neu.madcourse.apurvasharma.trickiest_part.TRICKMENU"));

            }
        });

        Button finalProject = (Button) findViewById(R.id.final_project);
        finalProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent("edu.neu.madcourse.apurvasharma.final_project.FINALPRJLAUNCHER"));

            }
        });

    }


    private SharedPreferences getPreferences() {
        return getSharedPreferences(MainMenu.class.getSimpleName(),
                MainMenu.this.MODE_PRIVATE);
    }

    public static SharedPreferences returnSharedPref() {
        return storeData;
    }



}
