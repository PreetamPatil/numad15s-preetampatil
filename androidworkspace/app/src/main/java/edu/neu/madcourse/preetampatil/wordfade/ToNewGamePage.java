
package edu.neu.madcourse.preetampatil.wordfade;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.neu.madcourse.preetampatil.MainMenu;
import edu.neu.madcourse.preetampatil.R;
import edu.neu.madcourse.preetampatil.communication.SendMessage;
import edu.neu.madcourse.preetampatil.sudoku.Music;
import edu.neu.madcourse.preetampatil.twoPlayerWordGame.IsActivtyVisible;
import edu.neu.madcourse.preetampatil.twoPlayerWordGame.MySensor;
import edu.neu.madcourse.preetampatil.twoPlayerWordGame.YouLoose;
import edu.neu.madcourse.preetampatil.twoPlayerWordGame.YouWon;


/**
 * Created by Preetam on 2/14/2015.
 */

public class ToNewGamePage extends Activity {

    static GridView gameGrid;
    static List<String> list;
    static String[] array;
    static Dialog dialog;
    static Dialog gameOver;
    static int tileNumber;
    static ArrayAdapter ad;
    static List<Integer> tileData;
    static int horizontalTemp = 0;
    static int verticalTemp = 0;
    static String wordHorizontal = "";
    static String wordVertical = "";
    static List<String> correctWords;
    static SharedPreferences dictonary;
    boolean firstTime = true;
    boolean secondTime = false;
    List<Integer> tileAllowed;
    static calculatePoints calPoints;
    static Button points;
    static Button mute;
    int musicSel = 0;
    static int oneWord = 0;
    static boolean setPoints = false;
    private java.util.Timer t;
    Button fadeTime;
    static CountDownTimer countTime1;
    static List<Integer> currentList;
    static List<Integer> startTimerList;
    static List<Integer> fadeList1;
    static List<Integer> fadeList2;
    static boolean isFadeBoolean = false;
    static int checkIsFadeBoolean = 0;
    static List<Integer> myStartTimerList;
    static Button pause;
    static int resume = 1;
    static int onPause = 0;
    static Button quit;
    static Button split;
    static Random ranGen;
    static String letter;
    static boolean setSplitflag = false;
    static Button submit;
    static HashMap<List<Integer>, Integer> wordMap;
    static HashMap<List<Integer>, Integer> shakeList;
    List<List<Integer>> tempList;
    static boolean set = true;
    static boolean checkvalue;
    static Button dump;
    static int dumpCount = 0;
    static String[] dumpList;
    static boolean dumpFlag = false;
    static boolean dumpPressed = false;
    static String[] alphabetsNewPage = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
            "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    static Context con;
    TextView scoreTable;
    static Button yourScore;
    Button opponentScore;
    static Button yourPoints;
    static Button opponentPoints;
    static Boolean multiplayerFlag;
    String multiName;
    String multiRegId;
    static int currentOpponentScore = 0;
    int timerValue;
    static boolean submitPressed = false;
    static Intent intent;
    static Intent intent2;
    static Intent intent3;
    static Intent intent4;
    static Intent intent5;
    MySensor sensor;
    SensorManager mySensor;
    private int shakeNumber = 0;
    private Button shake;
    static int myScore;
    boolean isQuitPressed = false;
    static boolean checkflag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gamegrid);

        startClock();
        con = ToNewGamePage.this;

        multiName = getIntent().getStringExtra("Key");
        multiRegId = getIntent().getStringExtra("Value");

        intent = new Intent(ToNewGamePage.this, SendMessage.class);
        intent2 = new Intent(ToNewGamePage.this, SendMessage.class);
        intent3 = new Intent(ToNewGamePage.this, YouWon.class);
        intent4 = new Intent(ToNewGamePage.this, YouLoose.class);

        dictonary = getDictPreferences();


        final ProgressDialog progress;


        progress = ProgressDialog.show(this,"Loading","Please wait data is loading",true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {

                    if (!dictonary.getString(""+"z", null).equals(null))
                    {
                        break;
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                    }
                });
            }
        }).start();






        scoreTable = (TextView) findViewById(R.id.score_table);
        yourScore = (Button) findViewById(R.id.your_score);
        opponentScore = (Button) findViewById(R.id.opponent_score);
        yourPoints = (Button) findViewById(R.id.your_current_score);
        opponentPoints = (Button) findViewById(R.id.opponent_current_score);
        points = (Button) findViewById(R.id.points_button);



        if (getIntent().getStringExtra("Player").equals("single_player")) {
            scoreTable.setVisibility(View.GONE);
            yourScore.setVisibility(View.GONE);
            opponentScore.setVisibility(View.GONE);
            yourPoints.setVisibility(View.GONE);
            opponentPoints.setVisibility(View.GONE);
            multiplayerFlag = false;
        } else {
            points.setVisibility(View.GONE);
            multiplayerFlag = true;
        }

        ranGen = new Random();

        gameGrid = (GridView) findViewById(R.id.gridView1);
        array = new String[100];
        for (int i = 0; i < array.length; i++) {
            array[i] = "";
        }


        wordMap = new HashMap<List<Integer>, Integer>();
        shakeList = new HashMap<List<Integer>, Integer>();

        dumpList = new String[3];

        checkvalue = false;
        gameOver = new TimeOver(ToNewGamePage.this);
        list = new ArrayList<String>(Arrays.asList(array));
        fadeTime = (Button) findViewById(R.id.fade_button);
        tileData = new ArrayList<Integer>();
        correctWords = new ArrayList<String>();
        tileAllowed = new ArrayList<Integer>();
        currentList = new ArrayList<Integer>();
        startTimerList = new ArrayList<Integer>();
        fadeList1 = new ArrayList<Integer>();
        fadeList2 = new ArrayList<Integer>();
        myStartTimerList = new ArrayList<Integer>();
        tempList = new ArrayList<List<Integer>>();
        calPoints = new calculatePoints();
        Music.play(this, R.raw.wordfade_music);
        shake = (Button) findViewById(R.id.shake_button);
        shake.setVisibility(View.GONE);

        points.setText("" + 0);
        // setPointsMultiplayer(0);
        dialog = new KeyboardInput(ToNewGamePage.this, ToNewGamePage.this);


        sensor = new MySensor(ToNewGamePage.this);
        mySensor = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mySensor.registerListener(sensor.mySensorEventListener, mySensor
                        .getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);


        pause = (Button) findViewById(R.id.pause_button);
        quit = (Button) findViewById(R.id.quit_button);


        final MediaPlayer wrongWord = MediaPlayer.create(this, R.raw.wrongword);
        final MediaPlayer correctAns = MediaPlayer.create(this, R.raw.correct_ans);


        dump = (Button) findViewById(R.id.dump_button);
        dump.setVisibility(View.INVISIBLE);


        dump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dumpPressed = true;
                if (KeyboardInput.checkInvisible.size() < 5) {

                    dump.setVisibility(View.INVISIBLE);

                }
                dialog.show();
            }
        });

        split = (Button) findViewById(R.id.split_button);
        split.setVisibility(View.INVISIBLE);

        split.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                while (true) {

                    int r = ranGen.nextInt(alphabetsNewPage.length);
                    if (KeyboardInput.randomCount[r] < KeyboardInput.alphabetsCount[r]) {
                        KeyboardInput.randomCount[r] = KeyboardInput.randomCount[r] + 1;
                        letter = alphabetsNewPage[r];
                        break;
                    }
                }

                setSplitflag = true;
                Toast.makeText(getApplicationContext(), "Letter " + letter + " has been added in your Rack", Toast.LENGTH_SHORT).show();

                if (KeyboardInput.checkInvisible.size() >= 1 && KeyboardInput.checkInvisible.size() <= 10) {

                    int score = Integer.parseInt("" + points.getText());
                    score = score - 5;
                    if (score < 0) {
                        gameOver();
                        submit.setVisibility(View.INVISIBLE);
                        split.setVisibility(View.INVISIBLE);
                        dump.setVisibility(View.INVISIBLE);

                    }
                    points.setText(score + "");
                    setPointsMultiplayer(score);
                    Toast.makeText(getApplicationContext(), "Your score has been deducted by 5 points", Toast.LENGTH_SHORT).show();
                }

                if (KeyboardInput.checkInvisible.size() == 17) {

                    int score = Integer.parseInt("" + points.getText());
                    score = score + 4;
                    points.setText(score + "");
                    setPointsMultiplayer(score);
                    Toast.makeText(getApplicationContext(), "Your score increased by 4 points", Toast.LENGTH_SHORT).show();
                }
                if (KeyboardInput.checkInvisible.size() == 18) {

                    int score = Integer.parseInt("" + points.getText());
                    score = score + 6;
                    points.setText(score + "");
                    setPointsMultiplayer(score);
                    Toast.makeText(getApplicationContext(), "Your score increased by 6 points ", Toast.LENGTH_SHORT).show();
                }

                if (KeyboardInput.checkInvisible.size() == 19) {

                    int score = Integer.parseInt("" + points.getText());
                    score = score + 8;
                    points.setText(score + "");
                    setPointsMultiplayer(score);
                    Toast.makeText(getApplicationContext(), "Your score increased by 8 points", Toast.LENGTH_SHORT).show();
                }
                if (KeyboardInput.checkInvisible.size() == 20) {

                    int score = Integer.parseInt("" + points.getText());
                    score = score + 10;
                    points.setText(score + "");
                    setPointsMultiplayer(score);
                    Toast.makeText(getApplicationContext(), "Your score increased by 10 points", Toast.LENGTH_SHORT).show();
                }
                if (KeyboardInput.checkInvisible.size() == 21) {

                    int score = Integer.parseInt("" + points.getText());
                    score = score + 15;
                    points.setText(score + "");
                    setPointsMultiplayer(score);
                    Toast.makeText(getApplicationContext(), "Your score increased by 21 points", Toast.LENGTH_SHORT).show();
                }

            }
        });

        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IsActivtyVisible.activityPaused();
                isQuitPressed = true;
                setPointsMultiplayer(-1);
                onBackPressed();

            }
        });

        pause.setText("Pause");
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                while (true) {

                    if (resume == 1) {
                        gameGrid.setVisibility(View.INVISIBLE);
                        pause.setText("Resume");
                        submitPressed = false;
                        onPause = 1;
                        resume = 0;
                        break;
                    }

                    if (onPause == 1) {

                        pause.setText("Pause");
                        gameGrid.setVisibility(View.VISIBLE);
                        submitPressed = true;
                        onPause = 0;
                        resume = 1;
                        break;

                    }
                }
            }
        });


        mute = (Button) findViewById(R.id.mute_button);
        mute.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                while (true) {
                    if (musicSel == 0) {
                        mute.setBackgroundDrawable(getResources().getDrawable(R.drawable.mute));
                        Music.stop(ToNewGamePage.this);
                        musicSel = 1;
                        break;
                    }

                    if (musicSel == 1) {
                        mute.setBackgroundDrawable(getResources().getDrawable(R.drawable.unmute));
                        Music.play(ToNewGamePage.this, R.raw.wordfade_music);
                        musicSel = 0;
                        break;
                    }

                }
            }
        });


        ad = new ArrayAdapter<String>(this, R.layout.gridstyle, list);
        gameGrid.setAdapter(ad);
        gameGrid.setNumColumns(10);

        gameGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                if (tileData.size() == 0) {
                    tileAllowed.clear();
                    secondTime = false;
                    firstTime = true;
                }

                if (!tileAllowed.contains(i) && secondTime) {
                    Toast.makeText(getApplicationContext(), "Please select correct tile", Toast.LENGTH_LONG).show();
                }


                if (firstTime) {
                    tileNumber = i;
                    dialog.show();
                    firstTime = false;
                    secondTime = true;

                    if (i % 10 == 0) {

                        tileAllowed.add(i + 1);
                        tileAllowed.add(i - 10);
                        tileAllowed.add(i + 10);
                    } else if ((i + 1) % 10 == 0) {


                        tileAllowed.add(i - 1);
                        tileAllowed.add(i - 10);
                        tileAllowed.add(i + 10);

                    } else {

                        tileAllowed.add(i - 1);
                        tileAllowed.add(i + 1);
                        tileAllowed.add(i - 10);
                        tileAllowed.add(i + 10);
                    }

                }

                if (tileAllowed.contains(i)) {
                    tileNumber = i;
                    dialog.show();
                    firstTime = false;
                    if (i % 10 == 0) {
                        tileAllowed.add(i + 1);
                        tileAllowed.add(i - 10);
                        tileAllowed.add(i + 10);

                    } else if ((i + 1) % 10 == 0) {
                        tileAllowed.add(i - 1);
                        tileAllowed.add(i - 10);
                        tileAllowed.add(i + 10);
                    } else {
                        tileAllowed.add(i - 1);
                        tileAllowed.add(i + 1);
                        tileAllowed.add(i - 10);
                        tileAllowed.add(i + 10);
                    }
                }

            }

        });


        submit = (Button) findViewById(R.id.submit_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkIsFadeBoolean++;
                setPoints = false;
                Log.w("CheckIsfadeBoolean", checkIsFadeBoolean + "");
                if (checkIsFadeBoolean == 2) {

                    isFadeBoolean = true;
                }


                while (true) {

                    Collections.sort(tileData);
                    for (int i = 0; i < tileData.size(); i++) {

                        int temp = tileData.get(i);
                        Log.w("Temp: ", "" + temp);
                        ////////////////////Horizontal check for zero position//////////////////////////
                        if (temp == 0) {

                            horizontalTemp = temp;

                            while (list.get(horizontalTemp + 1) != "") {
                                wordHorizontal = wordHorizontal.concat(list.get(horizontalTemp));
                                currentList.add(horizontalTemp);
                                horizontalTemp = horizontalTemp + 1;
                                if (list.get(horizontalTemp + 1).equals("")) {
                                    wordHorizontal = wordHorizontal.concat(list.get(horizontalTemp));
                                    currentList.add(horizontalTemp);
                                }

                            }
                            Log.w("Horizontal word: ", wordHorizontal);
                            if (correctWords.contains(wordHorizontal)) {
//                                Toast.makeText(getApplicationContext(), "Words cannot be repeated", Toast.LENGTH_LONG).show();
                                currentList.clear();
                            }

                            if (!correctWords.contains(wordHorizontal)) {
                                if (wordHorizontal.length() >= 1) {
                                    String words = dictonary.getString("" + wordHorizontal.toLowerCase().charAt(0), null);
                                    List<String> listWord = Arrays.asList(words.split(","));

                                    if (listWord.contains(" " + wordHorizontal.toLowerCase())) {
                                        if (oneWord == 1) {
                                            Toast.makeText(getApplicationContext(), "Correct Word", Toast.LENGTH_LONG).show();
                                            correctWords.add(wordHorizontal);
                                            correctAns.start();
                                            for (int in = 0; in < currentList.size(); in++) {
                                                gameGrid.getChildAt(currentList.get(in)).setBackgroundColor(getResources().getColor(R.color.green));
                                            }
                                            startTimer(currentList);
                                            oneWord = 0;

                                            wordHorizontal = "";
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Only one Correct Word allowed", Toast.LENGTH_LONG).show();
                                            currentList.clear();
                                            setPoints = true;
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), wordHorizontal + "  is not a word", Toast.LENGTH_LONG).show();
                                        wordHorizontal = "";
                                        wrongWord.start();
                                        for (int in = 0; in < currentList.size(); in++) {
                                            gameGrid.getChildAt(currentList.get(in)).setBackgroundColor(getResources().getColor(R.color.red));
                                        }
                                        setPoints = true;
                                    }
                                }
                            }

                        }
                        ////////////////////Horizontal check for zero position Ends//////////////////////////

                        ////////////////////Horizontal check Starts//////////////////////////

                        if (temp > 0) {

                            if (list.get(temp - 1).equals("")) {
                                //For horizontal words
                                horizontalTemp = temp;

                                while (list.get(horizontalTemp + 1) != "") {
                                    wordHorizontal = wordHorizontal.concat(list.get(horizontalTemp));
                                    currentList.add(horizontalTemp);
                                    horizontalTemp = horizontalTemp + 1;
                                    if (list.get(horizontalTemp + 1).equals("")) {
                                        wordHorizontal = wordHorizontal.concat(list.get(horizontalTemp));
                                        currentList.add(horizontalTemp);
                                    }

                                }
                                Log.w("Horizontal word: ", wordHorizontal);

                                if (correctWords.contains(wordHorizontal)) {
                                    //                                  Toast.makeText(getApplicationContext(), "Words cannot be repeated",
                                    //Toast.LENGTH_LONG).show();
                                }

                                if (!correctWords.contains(wordHorizontal)) {
                                    if (wordHorizontal.length() >= 1) {
                                        String words = dictonary.getString("" + wordHorizontal.toLowerCase().charAt(0), null);
                                        List<String> listWord = Arrays.asList(words.split(","));
                                        Log.w("Size of dictionary in Horizontal: ", listWord.size() + "");
                                        if (listWord.contains(" " + wordHorizontal.toLowerCase())) {

                                            if (oneWord == 1) {
                                                oneWord = 0;
                                                Log.w("Word matched", "");
                                                Toast.makeText(getApplicationContext(), "Correct Word", Toast.LENGTH_LONG).show();
                                                correctWords.add(wordHorizontal);
                                                correctAns.start();
                                                for (int in = 0; in < currentList.size(); in++) {
                                                    gameGrid.getChildAt(currentList.get(in)).setBackgroundColor(getResources().getColor(R.color.green));
                                                }
                                                startTimer(currentList);
                                                wordHorizontal = "";
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Only one Correct Word allowed", Toast.LENGTH_LONG).show();
                                                setPoints = true;
                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), wordHorizontal + " is not a word", Toast.LENGTH_LONG).show();
                                            wordHorizontal = "";
                                            wrongWord.start();
                                            for (int in = 0; in < currentList.size(); in++) {
                                                gameGrid.getChildAt(currentList.get(in)).setBackgroundColor(getResources().getColor(R.color.red));
                                            }
                                            currentList.clear();
                                            setPoints = true;
                                        }
                                    }
                                }
                            }
                        }
                        ////////////////////Horizontal check Ends//////////////////////////

                        ////////////////////Vertical check for less than 10 Starts//////////////////////////

                        if (temp < 10) {

                            verticalTemp = temp;

                            while (list.get(verticalTemp + 10) != "") {
                                wordVertical = wordVertical.concat(list.get(verticalTemp));
                                currentList.add(verticalTemp);
                                verticalTemp = verticalTemp + 10;
                                if (list.get(verticalTemp + 10).equals("")) {
                                    wordVertical = wordVertical.concat(list.get(verticalTemp));
                                    currentList.add(verticalTemp);
                                }
                            }

                            Log.w("Vertical word: ", wordVertical);

                            if (correctWords.contains(wordVertical)) {

                                //                            Toast.makeText(getApplicationContext(), "Words cannot be repeated", Toast.LENGTH_LONG).show();

                            }

                            if (!correctWords.contains(wordVertical)) {

                                if (wordVertical.length() >= 1) {
                                    String words = dictonary.getString("" + wordVertical.toLowerCase().charAt(0), null);
                                    List<String> listWord = Arrays.asList(words.split(","));
                                    Log.w("Size of dictionary in vertical: ", listWord.size() + "");

                                    if (listWord.contains(" " + wordVertical.toLowerCase())) {

                                        if (oneWord == 1) {
                                            oneWord = 0;
                                            Toast.makeText(getApplicationContext(), "Correct Word", Toast.LENGTH_LONG).show();
                                            correctWords.add(wordVertical);
                                            correctAns.start();
                                            for (int in = 0; in < currentList.size(); in++) {
                                                gameGrid.getChildAt(currentList.get(in)).setBackgroundColor(getResources().getColor(R.color.green));
                                            }
                                            startTimer(currentList);
                                            currentList.clear();
                                            wordVertical = "";
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Only one  correct word allowed", Toast.LENGTH_LONG).show();
                                            setPoints = true;
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), wordVertical + " is not a Word", Toast.LENGTH_LONG).show();
                                        wordVertical = "";
                                        wrongWord.start();
                                        for (int in = 0; in < currentList.size(); in++) {
                                            gameGrid.getChildAt(currentList.get(in)).setBackgroundColor(getResources().getColor(R.color.red));
                                        }
                                        setPoints = true;
                                    }

                                }
                            }

                        }

                        ////////////////////Vertical check for less than 10 Starts ends//////////////////////////

                        ////////////////////Vertical check Starts //////////////////////////


                        if (temp > 9 && temp < 90) {

                            if (list.get(temp - 10).equals("")) {
                                //For vertical words

                                verticalTemp = temp;

                                while (list.get(verticalTemp + 10) != "") {
                                    wordVertical = wordVertical.concat(list.get(verticalTemp));
                                    currentList.add(verticalTemp);
                                    verticalTemp = verticalTemp + 10;
                                    if (list.get(verticalTemp + 10).equals("")) {
                                        currentList.add(verticalTemp);
                                        wordVertical = wordVertical.concat(list.get(verticalTemp));
                                    }
                                }

                                Log.w("Vertical word: ", wordVertical);

                                if (correctWords.contains(wordVertical)) {

                                    //                              Toast.makeText(getApplicationContext(), "Words cannot be repeated",
                                   // Toast.LENGTH_LONG).show();

                                }

                                if (!correctWords.contains(wordVertical)) {
                                    if (wordVertical.length() >= 1) {
                                        String words = dictonary.getString("" + wordVertical.toLowerCase().charAt(0), null);
                                        List<String> listWord = Arrays.asList(words.split(","));

                                        if (listWord.contains(" " + wordVertical.toLowerCase())) {
                                            if (oneWord == 1) {
                                                oneWord = 0;
                                                Toast.makeText(getApplicationContext(), "Correct Word", Toast.LENGTH_LONG).show();
                                                correctWords.add(wordVertical);
                                                correctAns.start();
                                                startTimer(currentList);
                                                for (int in = 0; in < currentList.size(); in++) {
                                                    gameGrid.getChildAt(currentList.get(in)).setBackgroundColor(getResources().getColor(R.color.green));
                                                }
                                                currentList.clear();
                                                wordVertical = "";
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Only one  correct word allowed", Toast.LENGTH_LONG).show();
                                                setPoints = true;
                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), wordVertical + " is not a Word", Toast.LENGTH_LONG).show();
                                            wordVertical = "";
                                            wrongWord.start();
                                            for (int in = 0; in < currentList.size(); in++) {
                                                gameGrid.getChildAt(currentList.get(in)).setBackgroundColor(getResources().getColor(R.color.red));
                                            }
                                            setPoints = true;
                                        }


                                    }
                                }
                            }
                        }
                        ////////////////////Vertical check Starts ends//////////////////////////

                        wordVertical = "";
                        wordHorizontal = "";
                        currentList.clear();
                    }

                    if (setPoints) {
                        break;
                    } else {
                        int points = calPoints.score(list);
                        setPoints(points);
                        break;
                    }

                }

            }
        });
    }

    public void setLetter(String a) {


        Log.w("Title number: ", tileNumber + "");
        Log.w("String: ", a);
        setText(tileNumber, a);
        setText(tileNumber, a);

    }

    public void setText(int tileNumber, String letter) {

        Log.w("Tilesize: ", +tileData.size() + "");

        if (KeyboardInput.checkInvisible.size() == 3) {

            dump.setVisibility(View.VISIBLE);

        }

        if (KeyboardInput.checkInvisible.size() >= 6) {

            dump.setVisibility(View.VISIBLE);
        }

        if (dumpPressed) {
            dumpText(letter);
            dumpPressed = false;
        } else {
            oneWord = 1;
            setPoints = false;
            checkvalue = true;
            split.setVisibility(View.VISIBLE);
            tileData.add(tileNumber);
            list.set(tileNumber, letter);
            ad.notifyDataSetChanged();
        }

    }

    public void setPoints(int point) {

        points.setText("" + point);
        setPointsMultiplayer(point);

    }

    @Override
    protected void onPause() {
        super.onPause();
        IsActivtyVisible.activityPaused();
        Music.stop(ToNewGamePage.this);

        Log.w("I am in onPause","");
        if (checkflag){
            Log.w("I am in onPause1","");
            onBackPressed();
        }
        mute.setBackgroundDrawable(getResources().getDrawable(R.drawable.mute));


    }

    @Override
    protected void onResume() {
        super.onResume();
        IsActivtyVisible.activityResumed();
        mute.setBackgroundDrawable(getResources().getDrawable(R.drawable.unmute));
        Music.play(ToNewGamePage.this, R.raw.wordfade_music);

        if (checkflag){
            Log.w("I am in onResume1","");
            onBackPressed();
        }

    }

    public void fadeTimer1() {

        Log.w("I am in: ", "");

        String word = "";
        Log.w("After--------------", "-------------------");
        if (wordMap.size() > 0) {
            for (Map.Entry<List<Integer>, Integer> entry5 : wordMap.entrySet()) {
                List<Integer> key = entry5.getKey();
                int a = wordMap.get(key);
                a = a - 1;
                wordMap.put(key, a);
                Log.w("Key value: ", "" + key);
                if (a == 20) {

                    if (shakeNumber <= 3) {
                        shake.setVisibility(View.VISIBLE);
                    }

                    if (!shakeList.containsKey(key)) {
                        shakeList.put(key, 60);
                    }
                    for (int i = 0; i < key.size(); i++) {
                        gameGrid.getChildAt(key.get(i)).setBackgroundColor(getResources().getColor(R.color.green_fade));
                        word = word.concat(list.get(key.get(i)));
                        Log.w("Tile numnber: ", "" + key.get(i));
                    }
                    Toast.makeText(getApplicationContext(), "Word " + word + " is about to fade!!", Toast.LENGTH_SHORT).show();
                }

                Log.w("Key value in timer = ", "" + key);
                Log.w("Values in timer = ", "" + a);
            }
            Log.w("-------------------", "-------------------");

        }

        for (Map.Entry<List<Integer>, Integer> entry5 : wordMap.entrySet()) {
            List<Integer> key = entry5.getKey();
            Integer a = wordMap.get(key);

            if (a <= 1) {
                for (int i = 0; i < key.size(); i++) {
                    Log.w("TileValue: ", key.get(i) + "");
                    gameGrid.getChildAt(key.get(i)).setBackgroundColor(getResources().getColor(R.color.grid_color));
                    list.set(key.get(i), "");
                    ad.notifyDataSetChanged();
                }
                tempList.add(key);
            }

        }

        for (int i = 0; i < tempList.size(); i++) {
            wordMap.remove(tempList.get(i));
        }
        tempList.clear();

        if (wordMap.size() == 0) {
            // gameOver();
            Log.w("inside 1", "");
            //countTime1.cancel();
            set = false;
        } else {
            // countTime1.start();
        }

    }

    public void startTimer(List<Integer> currentList) {

        Log.w("Current list: ", currentList + "");
        Log.w("WordMap size: ", "" + wordMap.size());


        for (int i = 0; i < currentList.size(); i++) {

            if (wordMap.size() > 0) {
                Log.w("After--------------", "-------------------");
                for (Map.Entry<List<Integer>, Integer> entry5 : wordMap.entrySet()) {
                    List<Integer> key = entry5.getKey();
                    if (key.contains(currentList.get(i))) {
                        wordMap.put(key, 60);
                        for (int in = 0; in < key.size(); in++) {
                            gameGrid.getChildAt(key.get(in)).setBackgroundColor(getResources().getColor(R.color.green));
                        }
                    }
                    Integer a = wordMap.get(key);
                    Log.w("Key in startTimer= ", "" + key);
                    Log.w("Values int startTimer = ", "" + a.toString());
                }
                Log.w("-------------------", "-------------------");
                timerValue = 60;
            } else {
                // fadeTime.setText("30");
                wordMap.put(new ArrayList<Integer>(currentList), 60);
                //fadeTimer1();
                timerValue = 60;
                submitPressed = true;
                Log.w("WordMap size: ", "" + wordMap.size());
                break;
            }
        }

        if (isFadeBoolean) {
            wordMap.put(new ArrayList<Integer>(currentList), 60);

        }

    }

    public static String splitText() {

        setSplitflag = false;
        return letter;
    }


    public void dumpText(String letter) {


        dumpCount = 0;
        Arrays.fill(dumpList, null);

        while (dumpCount < 3) {
            int r = ranGen.nextInt(alphabetsNewPage.length);
            if (alphabetsNewPage[r] != letter) {
                if (KeyboardInput.randomCount[r] < KeyboardInput.alphabetsCount[r]) {
                    KeyboardInput.randomCount[r] = KeyboardInput.randomCount[r] + 1;
                    dumpList[dumpCount] = alphabetsNewPage[r];
                    dumpCount++;
                }
            }
        }


        int score1 = calPoints.score(Arrays.asList(dumpList));
        score1 = score1 * 2;

        score1 = Integer.parseInt(points.getText().toString()) - 5 - score1;
        points.setText(score1 + "");

        Log.w("This is score1: ", "" + score1);
        dumpScore(score1);

        Log.w("Score: ", "" + score1);

        if (score1 < 0) {
            if (multiplayerFlag){
            }else{
                gameOver();
            }
            submit.setVisibility(View.INVISIBLE);
            split.setVisibility(View.INVISIBLE);
            dump.setVisibility(View.INVISIBLE);

        } else {
            Toast.makeText(con, "Letters " + dumpList[0] + "," + dumpList[1] + " and " + dumpList[2] +
                    " has been added in your Rack", Toast.LENGTH_LONG).show();
            dumpFlag = true;

        }
    }

    public static String[] getNewText() {

        dumpFlag = false;
        return dumpList;

    }

    public static void setValue(int i) {

        Log.w("This is value of i: ", "" + i);
        currentOpponentScore = i;
    }

    public int getValue() {

        Log.w("This is value of score: ", "" + currentOpponentScore);
        return currentOpponentScore;
    }

    public void setPointsMultiplayer(int points) {


        if (multiplayerFlag) {

            Log.w("This is points: ", "" + points);

            yourPoints.setText("" + points);

            myScore = points;

            intent.putExtra("Key", multiName);
            intent.putExtra("Value", multiRegId);
            intent.putExtra("classname", "score");
            intent.putExtra("open_activity", "false");
            intent.putExtra("scoreOfPlayer", "" + points);

            if (intent != null) {
                ToNewGamePage.this.startActivity(intent);
            }

            if (isQuitPressed || myScore < 0){
                gameOver();
            }
        }
    }

    public static SharedPreferences returnSharedPref() {
        return dictonary;
    }

    private SharedPreferences getDictPreferences() {
        return getSharedPreferences(MainMenu.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    public void startClock() {

        countTime1 = new CountDownTimer(6000000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                if (submitPressed) {
                    setTimerText();
                }
                opponentPoints.setText("" + getValue());
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    public void setTimerText() {

        fadeTime.setText("" + timerValue);

        Log.w("Fade time: ", "" + timerValue);

        if (timerValue == 0) {
            Log.w("inside 2", "");
            gameOver();
        } else {
            fadeTimer1();
        }
        timerValue--;


    }

    public void gameOver() {

        Toast.makeText(con, "Game Over", Toast.LENGTH_SHORT).show();
        submitPressed = false;
        submit.setVisibility(View.INVISIBLE);
        countTime1.cancel();


        if (multiplayerFlag) {

            IsActivtyVisible.activityPaused();
            intent2.putExtra("Key", multiName);
            intent2.putExtra("Value", multiRegId);
            intent2.putExtra("classname", "score");
            intent2.putExtra("open_activity", "false");
            intent2.putExtra("scoreOfPlayer", "game_over");

            if (null != intent2) {
                ToNewGamePage.this.startActivity(intent2);
            }

           if (myScore > currentOpponentScore){
               ToNewGamePage.intent3.putExtra("youscore", "" + ToNewGamePage.myScore);
               ToNewGamePage.intent3.putExtra("opponentscore", "" + ToNewGamePage.currentOpponentScore);

               if (null != intent3) {
                   ToNewGamePage.con.startActivity(intent3);
               }
            checkflag = true;
           }else{

               ToNewGamePage.intent4.putExtra("youscore", "" + ToNewGamePage.myScore);
               ToNewGamePage.intent4.putExtra("opponentscore", "" + ToNewGamePage.currentOpponentScore);

               if (null != intent4) {
                   ToNewGamePage.con.startActivity(intent4);
               }
               checkflag = true;
           }

        }

    }


    public static void opponentGameOverText() {
        ToNewGamePage.opponentGameOver();

    }

    public static void opponentGameOver() {

        submitPressed = false;

       /* ToNewGamePage.submit.setVisibility(View.GONE);
        ToNewGamePage.countTime1.cancel();*/

        Log.w("myscore: ",""+myScore);
        Log.w("currentopponent: ",""+ToNewGamePage.currentOpponentScore);


        if (myScore > ToNewGamePage.currentOpponentScore){

            Log.w("YourScore new: ",""+ToNewGamePage.myScore);
            Log.w("Opponent new: ",""+ToNewGamePage.currentOpponentScore);

            ToNewGamePage.intent3.putExtra("youscore", "" + ToNewGamePage.myScore);
            ToNewGamePage.intent3.putExtra("opponentscore", "" + ToNewGamePage.currentOpponentScore);

            if (null != intent3) {
                ToNewGamePage.con.startActivity(intent3);
            }

            checkflag = true;

        }else{

            Log.w("YourScore new: ",""+ToNewGamePage.myScore);
            Log.w("Opponent new: ",""+ToNewGamePage.currentOpponentScore);

            ToNewGamePage.intent4.putExtra("youscore", "" + ToNewGamePage.myScore);
            ToNewGamePage.intent4.putExtra("opponentscore", "" + ToNewGamePage.currentOpponentScore);

            if (null != intent4) {
                ToNewGamePage.con.startActivity(intent4);
            }

            checkflag = true;

        }
    }

    public void dumpScore(int dumpscore) {
        setPointsMultiplayer(dumpscore);
    }

    public void shakeTile() {

        Log.w("shakelistsize: ", "" + shakeList.size());

        if (shakeNumber <= 3) {

            if (shakeList.size() != 0) {

                if (shakeList.size() > 0) {
                    Log.w("After--------------", "-------------------");
                    for (Map.Entry<List<Integer>, Integer> entry5 : shakeList.entrySet()) {
                        List<Integer> key = entry5.getKey();

                        for (int in = 0; in < key.size(); in++) {
                            gameGrid.getChildAt(key.get(in)).setBackgroundColor(getResources().getColor(R.color.green));
                        }
                        wordMap.put(key, 60);
                    }
                }

                shakeList.clear();
                shakeNumber++;
                timerValue = 60;
                shake.setVisibility(View.GONE);
                Toast.makeText(con, "Shake Detected: You have save the word from fading", Toast.LENGTH_LONG).show();

                if (shakeNumber == 3){
                    mySensor.unregisterListener(sensor.mySensorEventListener);
                }
            }
        }

    }

    public void backPressed(){
        onBackPressed();
    }
}